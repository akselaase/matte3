import numpy as np
from fractions import Fraction

def solve(matrix):
    # div #row by #coeff
    # sub (#row * #coeff) from #rnext
    steps = []
    rows = matrix.shape[0]
    for row in range(rows):
        # Move rows around so we get a non-zero in cell (row,row)
        if matrix[row, row] == 0:
            for rswap in range(row + 1, rows):
                if matrix[rswap, row] != 0:
                    steps.append((matrix.copy(), 'swap', row, rswap))
                    temp = matrix[row].copy()
                    matrix[row] = matrix[rswap]
                    matrix[rswap] = temp
                    break

        # Divide so the first entry is 1
        coeff = matrix[row, row]
        if coeff != 1 and coeff != 0:
            steps.append((matrix.copy(), 'div', row, coeff))
            matrix[row] /= coeff

        # Sub from the next rows such that they have leading zeroes
        for rnext in range(row + 1, rows):
            coeff = matrix[rnext, row]
            if coeff != 0:
                steps.append((matrix.copy(), 'sub', row, coeff, rnext))
                matrix[rnext] -= matrix[row] * coeff

    for row in range(rows - 1, 0, -1):
        for rprev in range(row - 1, -1, -1):
            coeff = matrix[rprev][row]
            if coeff != 0:
                steps.append((matrix.copy(), 'sub', row, coeff, rprev))
                matrix[rprev] -= matrix[row] * coeff

    steps.append((matrix.copy(), 'fin'))
    return matrix, steps

def typeset_matrix(matrix):
    yield '\\begin{bmatrix}'
    for row in matrix:
        yield ' & '.join(map(str, row)) + ' \\\\'
    yield '\\end{bmatrix}'

def typeset_array(matrix, cols_right=1):
    opts = '{@{}' + 'r' * (matrix.shape[1] - cols_right) + '|' + 'r' * cols_right + '@{}}'
    yield '\\left[ \\begin{array}' + opts
    for row in matrix:
        yield ' & '.join(map(str, row)) + ' \\\\'
    yield '\\end{array} \\right]'

def format_fraction(frac, prefix='', suffix=''):
    if frac.denominator == 1:
        middle = str(frac.numerator)
    else:
        middle = '\\frac{' + str(frac.numerator) + '}{' + str(frac.denominator) + '}'
    return '$ ' + prefix + middle + suffix + ' $'

def typeset_explanation(step):
    tag = ''
    if step[1] == 'div':
        row, coeff = step[2:]
        if coeff > 1:
            tag = 'Divider rad ' + str(row + 1) + ' med ' + format_fraction(Fraction(coeff))
        else:
            tag = 'Multipliser rad ' + str(row + 1) + ' med ' + format_fraction(Fraction(1, coeff))
    elif step[1] == 'sub':
        row, coeff, rnext = step[2:]
        if abs(coeff) != 1:
            if coeff > 0:
                tag = 'Subtraher (' + format_fraction(Fraction(coeff), suffix='\\ *') + ' rad ' + str(row + 1) + \
                    ') fra rad ' + str(rnext + 1)
            else:
                tag = 'Adder (' + format_fraction(Fraction(abs(coeff)), suffix='\\ *') + ' rad ' + str(row + 1) + \
                    ') til rad ' + str(rnext + 1)
        else:
            if coeff > 0:
                tag = 'Subtraher rad ' + str(row + 1) + \
                    ' fra rad ' + str(rnext + 1)
            else:
                tag = 'Adder rad ' + str(row + 1) + \
                    ' til rad ' + str(rnext + 1)
    elif step[1] == 'swap':
        row, rswap = step[2:]
        tag = 'Bytt om på rad {} og rad {}'.format(row, rswap)

    yield '\\tag{' + tag + '}'

def typeset_solve(eqn_system, cols_right=1):
    yield '\\begin{align*}'
    
    _, steps = solve(eqn_system.copy())

    # Have the original problem and the first step on the same line
    yield from typeset_array(eqn_system, cols_right)
    for i in range(len(steps) - 1):
        if i == 0:
            yield ' & \\sim '
        else:
            yield ' \\\\ & \\sim '
        yield from typeset_array(steps[i + 1][0], cols_right)
        yield from typeset_explanation(steps[i])

    yield '\\end{align*}'

def get_latex(eqn_system, cols_right=1):
    print('\n'.join(typeset_solve(eqn_system + Fraction(), cols_right)))
